﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace proyectoWeb.Models
{
    public class Contexto:DbContext

    {
        public Contexto() : base("vehiculo") { }
        public DbSet<Autos> Autos {get; set;}
        public DbSet<Clasificacion> Clasificacion { get; set; } //se debe agregar
    }
}