﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace proyectoWeb.Models
{
    public class Clasificacion
    {
        [Key]
        public int ID { get; set; }
        [Required] //requiere u es obligacion este campo
        [StringLength(30)] //tamaño maximo
        public string nombre { get; set; }
        public int cod_auto { get; set; } //construye formacion foranea
        [ForeignKey("cod_auto")]
        public virtual Autos fk_auto { get; set; }



    }
}