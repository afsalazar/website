﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace proyectoWeb.Models
{
    public class Autos
    {
        [Key]
        public int ID {get; set;}
        [Required]
        public String modelo {get; set;}
        [Required]
        public String marca { get; set; }
        [Required]
        public String color {get; set;}
        [Required]
        public String placa {get; set;} 
            
    }
}