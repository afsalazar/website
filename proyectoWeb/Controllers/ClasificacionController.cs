﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using proyectoWeb.Models;

namespace proyectoWeb.Controllers
{
    public class ClasificacionController : Controller
    {
        private Contexto db = new Contexto();

        // GET: /Clasificacion/
        public ActionResult Index()
        {
            var clasificacion = db.Clasificacion.Include(c => c.fk_auto);
            return View(clasificacion.ToList());
        }

        // GET: /Clasificacion/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Clasificacion clasificacion = db.Clasificacion.Find(id);
            if (clasificacion == null)
            {
                return HttpNotFound();
            }
            return View(clasificacion);
        }

        // GET: /Clasificacion/Create
        public ActionResult Create()
        {
            ViewBag.cod_auto = new SelectList(db.Autos, "ID", "marca");
            return View();
        }

        // POST: /Clasificacion/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="ID,nombre,cod_auto")] Clasificacion clasificacion)
        {
            if (ModelState.IsValid)
            {
                db.Clasificacion.Add(clasificacion);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.cod_auto = new SelectList(db.Autos, "ID", "modelo", clasificacion.cod_auto);
            return View(clasificacion);
        }

        // GET: /Clasificacion/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Clasificacion clasificacion = db.Clasificacion.Find(id);
            if (clasificacion == null)
            {
                return HttpNotFound();
            }
            ViewBag.cod_auto = new SelectList(db.Autos, "ID", "modelo", clasificacion.cod_auto);
            return View(clasificacion);
        }

        // POST: /Clasificacion/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="ID,nombre,cod_auto")] Clasificacion clasificacion)
        {
            if (ModelState.IsValid)
            {
                db.Entry(clasificacion).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.cod_auto = new SelectList(db.Autos, "ID", "modelo", clasificacion.cod_auto);
            return View(clasificacion);
        }

        // GET: /Clasificacion/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Clasificacion clasificacion = db.Clasificacion.Find(id);
            if (clasificacion == null)
            {
                return HttpNotFound();
            }
            return View(clasificacion);
        }

        // POST: /Clasificacion/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Clasificacion clasificacion = db.Clasificacion.Find(id);
            db.Clasificacion.Remove(clasificacion);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
